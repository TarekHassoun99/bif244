# !/usr/bin/env python

import random
# Comments are the same as Perl

# Tabs are important in Python
# Statements do not have to end with a semicolomn
# Print in 3.7 can take multiple parameters, and are separated automatically by spaces
print("Hello")
print("This", "is", "number", 1)

# Variables can be assigned any time. They do not have to be declared as scalar
x = 8
print(x)
x = "Hello"
print(x)

# Variables are case sensitive
X = "Hello2"
print(X)
print(x)

# Same escape sequences work
print("Line1\nLine2\n\\Line3")

# Variable types are automatically inferred
num1 = 2
num2 = 3
num = num1 + num2
print(num)
type(num)

: # Unlike perl, there is no $_ / default variable
# '_'
10 + 20

# Using the _ will print the last value
_

# Same mathmatical rules and operations, as Perl, apply in python
x = -(-6)
print(x)

# There are no increment as x++
x++

# Shorthand apporaches such as var+= or var-= work
x += 5
print(x)

# We have to call for a classical approach
x = x + 1
print(x)

# The x operator in perl is similar to * in Python
print("str" * 2)
x = "Hellowz"
x * 5
print(x * 5)

# rnadom is not a native function
random(5)

# We need to import the module with this function
# .random() will call a function that Return the next random floating point number in the range [0.0, 1.0)
random.random()

# Python, unlike perl, cannot automatically convert values
"6"-2

# Conversion must be explicity set
int("6") - 2

# Block in Python must be done via 'tabs'. The block is introduced via a ':' if and only
# if it applies in the syntax (if statement, loop, etc)
if True:
	print("This is True")
else:
	print("This is False")
if False:
	print("This is True")
else:
	print("This is False")

# Basic Dice Application
# randrange returns an integer between 1 and 6
dice1 = random.randrange(1, 6)
print("The value of dice 1 is ", dice1)
dice2 = random.randrange(1, 6)
print("The value of dice 2 is ", dice2)
dice3 = random.randrange(1, 6)
print("The value of dice 3 is ", dice3)
dice4 = random.randrange(1, 6)
print("The value of dice 4 is ", dice4)
dice5 = random.randrange(1, 6)
print("The value of dice 5 is ", dice5)
sum = dice1 + dice2 + dice3 + dice4 + dice5
print("Sum:", sum)
